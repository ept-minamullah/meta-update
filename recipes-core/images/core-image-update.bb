SUMMARY = "A small image just capable of allowing a device to boot."

#IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL} packagegroup-core-full-cmdline"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image
inherit extrausers

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"


IMAGE_INSTALL_append = "wpa-supplicant \
					nano \
					openssl \
					openssh \
					canutils \
					i2c-tools \
					networkmanager \
					haveged \
					util-linux-hwclock \
                    util-linux \
					swupdate \
					swupdate-www \
					hwrevision \
					barebox-targettools \
					mtd-utils \
					"
DEPENDS += "cpio"


