SUMMARY = "This recipe checks the bootloader of the Machine if the bootloader is otherthan Barebox then it will generate an error"
LICENSE = "CLOSED"

do_validate_variable() {
    if [ "${PREFERRED_PROVIDER_virtual/bootloader}" != "barebox" ]; then
        exit 1
    fi
}

addtask validate_variable before do_compile
