SUMMARY = "This is pre-OTA-update recipe"

LICENSE = "CLOSED"
SRC_URI = " \
        file://pre-OTA-update.sh \
        "
do_install(){
     install -d ${D}${bindir}
     install -m 0755 ${WORKDIR}/pre-OTA-update.sh ${D}${bindir}/
}
FILES_${PN} +="${bindir}/pre-OTA-update.sh"
