#!/bin/sh

# ================== Making Directory to /tmp ==================================

mkdir /tmp/update
cd /tmp/update

# ================== Making config file to /tmp/update =========================
touch config

# ================== writing arguments in Config ===============================

printf "#!/bin/sh\n global.boot.default=net\n global.bootchooser.targets=net nand" >> config
echo "config file is here"

# ================== Making Directory to /tmp/update ============================

mkdir boot
echo "making boot directory"
cd boot/

# ================== Making net file to /tmp/update/boot =========================

touch net

# ================= Write data to the file "net" =================================

cat <<EOF > net
#!/bin/sh

path="/mnt/tftp"

global.bootm.image="\${path}/zImage"

oftree="\${path}/oftree"
if [ -f "\${oftree}" ]; then
	global.bootm.oftree="\$oftree"
fi
# ================ Providing location of tftp server files =========================
nfsroot="/tftpboot"

ip_route_get -b \${global.net.server} global.linux.bootargs.dyn.ip

initramfs="\${path}/initramfs"
if [ -f "\${initramfs}" ]; then
	global.bootm.initrd="\$initramfs"
else
	global.linux.bootargs.dyn.root="root=/dev/nfs nfsroot=\$nfsroot,v3,tcp"
fi
EOF

echo "boot net is here"
cd ..

# ================== Making Directory to /tmp/update ============================

mkdir nv
echo "making nv directory"
cd nv/

# ================== Making files and write data to them in /tmp/update/nv =======
touch allow_color
echo "false" >> allow_color
touch boot.watchdog_timeout
echo "60" >> boot.watchdog_timeout
touch bootchooser.last_chosen
echo "1" >> bootchooser.last_chosen
touch bootchooser.net.priority
echo "1" >> bootchooser.net.priority
touch bootchooser.net.remaining_attempts
echo "2" >> bootchooser.net.remaining_attempts
touch dev.eth0.ipaddr
echo "please provide ip_address of device"
read IP_ADD
echo "ip address set to $IP_ADD"
echo "$IP_ADD" >> dev.eth0.ipaddr

touch dev.eth0.serverip
echo "please provide ip_address of tftp server"
read SERVER_IP
echo "server address set to $SERVER_IP"
echo "$SERVER_IP" >> dev.eth0.serverip

touch linux.bootargs.base
echo "consoleblank=0" >> linux.bootargs.base

touch linux.bootargs.rootfs
echo "rootwait ro fsck.repair=yes" >> linux.bootargs.rootfs

touch net.server
echo "$SERVER_IP" >> net.server

touch set
echo "<NULL>" >> set

# ================== Saving Environments Var. to NAND ============================

echo "Saving the Environment variables to NAND"
bareboxenv -s -v /tmp/update /dev/mtdblock6

# ================== Rebooting the board =========================================

echo "rebooting the system & go into the initramfs for updation"
reboot
