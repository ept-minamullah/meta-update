This README file contains information on the contents of the meta-update layer.

Please see the corresponding sections below for details.

# Dependencies

  URI: https://github.com/sbabic/meta-swupdate.git
  
  branch: Dunfell

# Table of Contents

  I. Layer compatibility  
  II. Clone meta-update layer to build  
  III. Add meta-update layer to build  
  IV. swupdate_%.bbappend  
  V. Pre_OTA_update.  
  VI. hwrevison  
  VII. bootloader-check

## I. Layer compatibility  


  This layer is compatible with Barebox only as swupdate supports only U-boot,Grub. It does not support the Barebox so purpose of this layer is to perform swupdation for the Barebox so that's why this layer is compatible if the bootloader is Barebox.

## II.  Clone meta-update layer to your build


  Run ' git clone "https://gitlab.com/ept-minamullah/meta-update.git" '

## III.  Add meta-update layer to your build


  Run ' bitbake-layers add-layer meta-update '


## IV. swupdate_%.bbappend

  This bbappend file saves the Swupdate configurations

## V. Pre_OTA_update.sh

  The purspose of this shell script is to change the environment variable of Barebox from Rootfs and save them back to NAND 

## VI. hwrevison

  Swupdate By default, extracted the hardware information is  from /etc/hwrevision file. The file should contain a single line in the following format:

<"boardname"> <"revision">

  This is reversion is defied in the sw-discription file so update this file before building the image.

## VII. bootloader-check

As this layer is compatible with the Barebox, this recipe checks the bootloader if bootloader is other than Barebox then it will give the error while building the image